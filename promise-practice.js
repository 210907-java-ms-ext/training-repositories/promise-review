
function makeRequest(){
    console.log("making request...")

    return new Promise((resolve, reject)=>{

        setTimeout(()=>{
            if(Math.random()>.2){
                resolve('{"message":"hello world"}');
            } else {
                reject("an error has occurred");
            }
        },500);
    
    });
}

function transformRequest(data){
    console.log("transforming request...");

    return new Promise((resolve,reject)=>{
        resolve(JSON.parse(data));
    })
}

/*
makeRequest() // returns a promise of the json message
.then(data=>transformRequest(data)) // returns a promise of the parsed json
.then(message=>console.log(message))
.catch(error=>console.error(error));
*/

async function makeAsyncRequest(){
    let requestResults = await makeRequest();
    console.log(requestResults);
    let parsedMessage = await transformRequest(requestResults);
    console.log(parsedMessage);
}